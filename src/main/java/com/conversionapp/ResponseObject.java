package com.conversionapp;


import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ResponseObject {
    private double value;

    public ResponseObject(double valueP) {
        value = valueP;
    }

    public ResponseObject() {
        this.value = -999;
    }
}
